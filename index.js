/*У меня на домашнем ПК не работает сжатие imagemin-mozjpeg, 
по этому использую imagemin-jpegtran. Не могу сказать что лучше. 
Если есть время затестите что лучше сожмет..

Заметил, что у меня некоторые JPG меняет ориентацию..

 */
const options = require("minimist")(process.argv.slice(2)),
    path = require("path"),
    recursive = require("recursive-readdir"),
    fs = require('fs'),
    imagemin = require("imagemin"),
    imageminPngquant = require("imagemin-pngquant"),
    //imageminMozjpeg = require('imagemin-jpegtran'),
    imageminMozjpeg = require('imagemin-mozjpeg'),
    ALLOWED_EXTENSIONS = [".png", ".jpg", ".jpeg"],
    INPUT_DIR = "../../images/",    //путь папки для компрессии
    OUTPUT_DIR = "./images-min",
    QUALITY_JPG = [80,90],          //качество jpg
    QUALITY_PNG = [80/100,90/100],  //качество png
    VIEW_INFO = true, //вкл-выкл информирование
    BENCH = 100;                    //размер пакета изображений, который за раз может оптимизироваться

let GLOBAL_START_TIME,
    ALL_COUNT = 0,
    END = false,
    GLOBAL_ERROR_IMG = 0,
    GLOBAL_DONE_IMG = 0,
    GLOBAL_ALL_FILES_SIZE_AFTER = 0,
    GLOBAL_ALL_FILES_SIZE_BEFOR = 0;


const create_all_dir = async (DIR) => {
    let items = fs.readdirSync(DIR) 
    if (items == undefined) return;
    items.forEach(async item => {
        let img, tmp = ""
        if (fs.existsSync(`${DIR}/${item}/`)) {        		
            let p = `${DIR}/${item}`
            let fullPath = p.replace(new RegExp(INPUT_DIR, 'g'), OUTPUT_DIR)
            if (!fs.existsSync(fullPath)) {
            	fs.mkdirSync(fullPath)
        	}
            create_all_dir(p)
            }
        })
}
const get_all_files = async (DIR) => {
    return await new Promise((resolve) => {
        recursive(DIR, async (err, files) => {
            if (!Array.isArray(files)) {
                console.error("No files found!");
                process.exit(1);
            }
            resolve(files)
        })
    })
}
const get_array_files = (files) => {
    arr = []
    files.forEach(function(file) {
        var fileParentDir = path.dirname(file);
        var relativeToInputDir = path.relative(INPUT_DIR, fileParentDir);
        var fileOutputDir = path.join(OUTPUT_DIR, relativeToInputDir);
        var extenstion = path.extname(file).toLowerCase();
        let img, tmp = ""
        relativeToInputDir.split('/').forEach(p => {
            p = `${tmp}/${p}`
            tmp = p
            fs.mkdir(path.join(OUTPUT_DIR, p), (err) => {})
        })
        if (ALLOWED_EXTENSIONS.indexOf(extenstion) === -1) {
            img = false
            let _in = `${file}`;
            let _out = `${fileOutputDir}/${path.basename(file)}`
            fs.copyFile(_in, _out, (err) => {});
            return
        } else {
            ALL_COUNT++
            img = true
            GLOBAL_ALL_FILES_SIZE_BEFOR += fs.statSync(file)["size"]
        }
        arr.push({ fileParentDir, relativeToInputDir, fileOutputDir, file, fileName: path.basename(file), img })
    })
    return arr
}

const get_start_info = (arr, a) => {
    VIEW_INFO && console.log(` ------------------------------------------------`)
    VIEW_INFO && console.log(`|Количество картинок:             ${arr.length}`)
    VIEW_INFO && console.log(`|Общий объем изображений:         ${((GLOBAL_ALL_FILES_SIZE_BEFOR/1024/1024).toFixed(3)).toLocaleString('ru')} Mбайт`)
    VIEW_INFO && console.log(`|Качество jpg:                    ${QUALITY_JPG}`)
    VIEW_INFO && console.log(`|Качество png:                    ${QUALITY_PNG}`)
    VIEW_INFO && console.log(`|Количество блоков:               ${a.length}`)
    VIEW_INFO && console.log(`|Размер блока:                    ${BENCH}`)
    VIEW_INFO && console.log(` ------------------------------------------------\n`)
}

const get_end_info = async (from) => {
    if (!END){
        END = true
        let time = (Date.now() - GLOBAL_START_TIME) / 1000
        let in_dir = await get_all_files(INPUT_DIR)
        let out_dir = await get_all_files(OUTPUT_DIR)
        let in_ = await get_all_files(INPUT_DIR)
        let out_ = await get_all_files(OUTPUT_DIR)
        let poterali_ = await poterali(in_,out_)
        VIEW_INFO && console.log(` ------------------------------------------------`)
        VIEW_INFO && console.log(`|Затрачено времени:               ${time} с`)
        VIEW_INFO && console.log(`|Общий объем изображений после:   ${((GLOBAL_ALL_FILES_SIZE_AFTER/1024/1024).toFixed(3)).toLocaleString('ru')} Mбайт`)
        VIEW_INFO && console.log(`|Компрессия:                      ${((GLOBAL_ALL_FILES_SIZE_BEFOR-GLOBAL_ALL_FILES_SIZE_AFTER)/GLOBAL_ALL_FILES_SIZE_BEFOR*100).toFixed(2)} %`)
        VIEW_INFO && console.log(`|Среднее скорость:                ${(ALL_COUNT/time).toFixed(1)} img/сек`)
        VIEW_INFO && console.log(`|Сжато изображений:               ${GLOBAL_DONE_IMG} шт`)
        VIEW_INFO && console.log(`|Не сжато изображений:            ${GLOBAL_ERROR_IMG} шт`)
        VIEW_INFO && console.log(`|Потеряно файлов:                 ${poterali_.length} шт`)
        VIEW_INFO && console.log(` ------------------------------------------------\n`)
    } 
}

const compress = (arr) => {
    let t,
        _count_ = arr[0].length,
        count = 0,
        done = []
    start = Date.now();
    arr[0].map((r, i) => {
        imagemin([r.file], r.fileOutputDir, {
            plugins: [
                imageminMozjpeg({
                    progressive: true,
                    quality: QUALITY_JPG
                }),
                imageminPngquant({
                    quality: QUALITY_PNG
                })
            ]
        }).then((file) => {
            try{
                if (arr.length>0 && arr[0].length>0 ) {arr[0].splice(i, 1)}
                GLOBAL_DONE_IMG++;
                done.push(i)
                GLOBAL_ALL_FILES_SIZE_AFTER += file[0].data.length
                count++;
                if (_count_ == count) {
                    arr.splice(0, 1)
                    if (arr.length != 0) {
                        VIEW_INFO && console.log(`Осталось блоков: ${arr.length} Затрачено на блок: ${(Date.now()-start)/1000}с`)
                        compress(arr)
                    } else {
                        get_end_info(1)
                    }
                } 
        } catch (err){console.log('----',err)}
            
        }).catch(e => {
            GLOBAL_ERROR_IMG++;
            VIEW_INFO && console.log(`Ошибка с файлом ${r.file} Копируем без компрессии.`)
            let _out = `${path.resolve()}/${r.file}`;
            let _in = `${path.resolve()}/${r.fileOutputDir}/${r.fileName}`
            GLOBAL_ALL_FILES_SIZE_AFTER += fs.statSync(_out).size
            fs.copyFile(_out, _in, (err) => {
                if (err) {
                    VIEW_INFO && console.log("Ошибка при копировании", err);
                }
            });
            count++;
            if (arr.length != 0) {
                count--;
                _count_--;
                done.forEach((q, w) => {
                    if (q < i) { i-- }
                    arr[0].splice(q, 1);
                    count--;
                    _count_--;
                })
                arr[0].splice(i, 1);
                if (arr[0].length == 0) {
                    arr.splice(0, 1)
                }
                if (_count_ == count || arr.length != 0) {
                    compress(arr)
                } else {
                	console.log(arr.length,_count_,count)
                    get_end_info(2)
                }
            } else {
                get_end_info(3)
            }
        })

    })
}


Object.defineProperty(Array.prototype, 'chunk_inefficient', {
    value: function(chunkSize) {
        var array = this;
        return [].concat.apply([],
            array.map(function(elem, i) {
                return i % chunkSize ? [] : [array.slice(i, i + chunkSize)];
            })
        );
    }
});


Array.prototype.diff = function(a) {
    return this.filter(function(i) { return a.indexOf(i) < 0; });
};





const poterali = async (in_,out_) =>{
    return await new Promise((resolve) => {
		let poterali = in_.filter((x,i) => {
			x = x.replace(/(\.\.\/)/g,"").replace(/(\.\/)/g,"")
            let tmp = x.split('/')
            tmp[0] = OUTPUT_DIR.slice(2, OUTPUT_DIR.length)
            tmp.splice(1,1) 
            x = tmp.join('/')
            return !out_.includes(x)
        })
	resolve(poterali)
	})
}


//----стартуем-----//
(async () => {
    let files = await get_all_files(INPUT_DIR)
    let arr = get_array_files(files)
    let a = arr.chunk_inefficient(BENCH)
    get_start_info(arr, a)
    VIEW_INFO && console.log("Копирую структуру папок..")
    create_all_dir(INPUT_DIR)
    VIEW_INFO && console.log("Готово!")
    VIEW_INFO && console.log("Начинаю компрессию..")
    GLOBAL_START_TIME = Date.now()
    compress(a)
})()

